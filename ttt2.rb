class TicTacToe

  def dun?(seq)
    subseq = seq.reverse.each_with_index.select{|s, i| i.even?}.transpose[0]
    [
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],
      [[0, 0], [1, 0], [2, 0]],
      [[0, 1], [1, 1], [2, 1]],
      [[0, 2], [1, 2], [2, 2]],
      [[0, 0], [1, 1], [2, 2]],
      [[2, 0], [1, 1], [0, 2]]
    ].any? {|x| (x-subseq).empty?}
  end

  def grind(unplayed, played)
    unplayed.each do |square|
      next_state = played+[square]
      if dun?(next_state)
        @sequences += 1
        if next_state.length.odd?
          @x += 1
        else
          @o += 1
        end
      elsif next_state.length == 9
        @sequences += 1
        @draws += 1
      else
        @subsequences += 1
        grind(unplayed-[square], next_state)
      end
    end
  end

  def initialize
    squares = [0,1,2].product([0,1,2])
    @draws = 0
    @x = 0
    @o = 0
    @sequences = 0
    @subsequences = 0
    grind(squares, [])
    puts "#{@sequences} complete game move sequences"
    puts "of which X wins #{@x} and O wins #{@o}"
    puts "#{@draws} sequences result in a draw"
    puts
    puts "#{@subsequences} incomplete game move sequences"
  end
end

TicTacToe.new